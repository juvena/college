### What is the point?

College is a project made in ionic framework. For more information, visit [ionic](http://ionicframework.com/docs/) documentation.


### With the Ionic CLI:

In the project directory run:

```bash
$ sudo npm install
```

To run ionic serve:
```bash
$ ionic serve
```
Alternatively, you can copy the www folder to an apache for example

To build in an device run:
```bash
$ ionic cordova run android
```

Substitute android for ios if not on a Android.

### References:
* [joshmorony](https://www.joshmorony.com/);
* [scotch.io](https://scotch.io/)