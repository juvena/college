import { Injectable } from '@angular/core';
import { Http, Headers, RequestOptions } from '@angular/http';

import {Observable} from 'rxjs/Observable';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/timeout';
 
export class User {
  cpf: string;
  name: string;
  semester: string;
  course: string;
  token: string;
 
  constructor(cpf, name, semester, course, token) {
    this.cpf = cpf;
    this.name = name;
    this.semester = semester;
    this.course = course;
    this.token = token;
  }
}
 
@Injectable()
export class LoginService {
  ERRORMSG = "Please insert credentials";
  USERURL = 'http://localhost:3000/api/user';
  currentUser: User;
  msg = null;
  options: any;
  
  constructor(public http: Http) {
    this.msg = null;
  }
  getOpts(){
    return this.options;
  }
  buildHeader(cpf, pass){
    var string = cpf.toString() + ":" + pass;
     // Encode the String
     var encodedString = btoa(string)
     var headers = new Headers();
     headers.append('Authorization', 'Basic ' + encodedString);
     let options = new RequestOptions({ headers: headers });
     this.options = options;
     return options;
  }

   public getRemoteUser(user, pass, observer){
    let options = this.buildHeader(user, pass);

    return this.http.get(this.USERURL, options)
      .map(res => res.json())
      .subscribe(data => {

        if (data) {
            this.msg = data;
            this.currentUser = new User(data.cpf, data.name, data.semester, data.degree_name, options);
        } else {
            this.msg = null;
        }
        observer.next({
            message: data,
            type: data.success
        });
                
      }, (err) => {
        this.msg = null;
        console.log(err)
        observer.next(false)
     });

  }
  public login(credentials) {
    
    if (credentials.cpf === null || credentials.pass === null) {
      return Observable.throw(this.ERRORMSG);
    } else {     
      return Observable.create(observer => {        
       this.getRemoteUser(credentials.cpf, credentials.pass, observer);
      });
    }
  }
 
  public register(credentials) {
    if (credentials.email === null || credentials.password === null) {
      return Observable.throw(this.ERRORMSG);
    } else {
      // At this point store the credentials to your backend!
      return Observable.create(observer => {
        observer.next(true);
        observer.complete();
      });
    }
  }
 
  public getUserInfo() : User {
    return this.currentUser;
  }
 
  public logout() {
    return Observable.create(observer => {
      this.currentUser = null;
      observer.next(true);
      observer.complete();
    });
  }
}