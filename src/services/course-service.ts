import { Injectable } from '@angular/core';

export class Course {
    course: string;
    professor: string;
    difficult: string;
    start_time: string;
    end_time: string

 
  constructor(course, professor, difficult, start_time, end_time) {
    this.course = course;
    this.professor = professor;
    this.difficult = difficult;
    this.start_time = start_time;
    this.end_time = end_time;
  }
}

@Injectable()
export class CourseService {
    //currentCourse: Course;
    currentCourse: any;

    public setCurrentCourse(course){
        this.currentCourse = course;
    }

    public getCourseInfo() : Course {
    return this.currentCourse;
  }
}