import { Component, ViewChild } from '@angular/core';
import { Nav, Platform } from 'ionic-angular';
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
import {Http} from '@angular/http';
import { LoginService } from '../services/login-service';

@Component({
  templateUrl: 'app.html'
})
export class MyApp {
  @ViewChild(Nav) nav: Nav;
  
  rootPage:any = 'LoginPage';
  auth: any;
  http: Http;

  pages: Array<{title: string, component: any}>
  info: any;

  constructor(platform: Platform, statusBar: StatusBar, splashScreen: SplashScreen, auth: LoginService, http: Http,) {
    this.http = http;
    this.info = auth.getUserInfo();

    platform.ready().then(() => {
      // Okay, so the platform is ready and our plugins are available.
      // Here you can do any higher level native things you might need.
      statusBar.styleDefault();
      splashScreen.hide();
    });
    
  }
}