import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';

import { LoginService } from '../../services/login-service';
import { CourseService } from '../../services/course-service';

import { HomePage } from '../home/home';

/**
 * Generated class for the CoursesPage page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */
@IonicPage()
@Component({
  selector: 'page-courses',
  templateUrl: 'courses.html',
})
export class CoursesPage {
  user_info = {name: '',  nick: ''};
  course: any;
  constructor(public navCtrl: NavController, public navParams: NavParams, private auth: LoginService, private course_det: CourseService) {
    this.course = course_det.getCourseInfo();
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad CoursesPage');
  }

  backHome(course){
    this.navCtrl.setRoot(HomePage)
  }

}
