import { Component } from '@angular/core';
import { Http } from '@angular/http';

import { Platform, NavController } from 'ionic-angular';

import { SupportPage } from '../support/support';
import { RecordsPage } from '../records/records';

import { LoginService } from '../../services/login-service';
import { CourseService } from '../../services/course-service';

@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {
  user_info = {name: '',  cpf: '', semester: '', course: ''};
  COURSESURL = 'http://localhost:3000/api/courses/';
  sideRight = "true";
  courses = [];
  pages: any;
  token: any;
  
  constructor(public navCtrl: NavController, public http: Http, private auth: LoginService, private detail: CourseService, public platform: Platform) {
    this.sideRight = this.defineToggle();
    this.pages = this.showMenu();

    let info = auth.getUserInfo();
    this.user_info.name = info['name']
    this.user_info.cpf = info['cpf']
    this.user_info.semester = info['semester']
    this.user_info.course = info['course']
    this.token = info['token']
    this.getSemesterCourses();
    //this.courses = json.semester;
  }
  logout() {
    this.auth.logout().subscribe(succ => {
      this.navCtrl.setRoot('LoginPage')
    });
  }

  getSemesterCourses(){

    return this.http.get(this.COURSESURL+this.user_info.semester, this.token)
      .map(res => res.json())
      .subscribe(data => {

        if (data) {
            this.courses = data;
        } else {
            this.courses = [];
        }
                
      }, (err) => {
        this.courses = [];
        console.log(err)
     });
  }

  showMenu(){
    let pages = [
         {title:'Histórico Escolar', component:RecordsPage},
         {title:'Disciplinas', component:HomePage},
         {title:'Suporte', component:SupportPage}
       ]
    return pages;
  }

  openPage(page){
     this.navCtrl.setRoot(page.component);
   }

  ngOnInit(){
    this.courses;
  }

  showDetail(course){
    this.detail.setCurrentCourse(course)
    this.navCtrl.setRoot('CoursesPage')
  }

  defineToggle(){
    console.log(this.platform.is('mobile'));
    if (this.platform.is('mobile')) {
      return "false"
    }else{
      return "true"
    }
  }

}
