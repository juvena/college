import { Component } from '@angular/core';
import {Http} from '@angular/http';
import { Platform, IonicPage, NavController, NavParams, Loading, LoadingController, AlertController, ToastController} from 'ionic-angular';

import { LoginService } from '../../services/login-service';
import { HomePage } from '../home/home'

@IonicPage()
@Component({
  selector: 'page-login',
  templateUrl: 'login.html',
})
export class LoginPage {
  OKMSG = "Login Realizado Com Sucesso!";
  errormsg = "Login/Senha inválido(s)";
  loading: Loading;
  registerCredentials = { cpf: '', pass: '' };
  http: Http;
  constructor(public navCtrl: NavController, public navParams: NavParams, public auth: LoginService, public platform: Platform, public alertCtrl: AlertController, public toastCtrl: ToastController, public loadingCtrl: LoadingController, http: Http) {
    this.defineMessages();
  }

  private defineMessages(){
    if (this.platform.is('mobile')) {
      this.errormsg = "Login Inválido"
    }
  }

  private defineErrorSpan(){
    if (this.platform.is('mobile')) {
      this.showToast(this.errormsg);
    }else{
      this.showError(this.errormsg);
    }
  }

  private showToast(message){
    let toast = this.toastCtrl.create({
      message: message,
      duration: 2000,
      position: 'top'
    });
    toast.present(toast);
  }

  public login() {
    this.showLoading()
    this.auth.login(this.registerCredentials).subscribe(allowed => {
      if (allowed) {
        this.showToast(this.OKMSG);
        this.navCtrl.setRoot(HomePage);
      } else {
        this.defineErrorSpan();
        this.navCtrl.setRoot(LoginPage);
      }
    },
      error => {
        this.defineErrorSpan();
      });
  }

  showLoading() {
    this.loading = this.loadingCtrl.create({
      content: 'Please wait...',
      dismissOnPageChange: true
    });
    this.loading.present();
  }
 
  showError(text) {
    this.loading.dismiss();
 
    let alert = this.alertCtrl.create({
      title: '',
      subTitle: text,
      buttons: ['OK']
    });
    alert.present(prompt);
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad LoginPage');
  }

}
