import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, Loading, LoadingController, AlertController} from 'ionic-angular';
import { Http } from '@angular/http';

import { HomePage } from '../home/home'

import { LoginService } from '../../services/login-service';

/**
 * Generated class for the SupportPage page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */
@IonicPage()
@Component({
  selector: 'page-support',
  templateUrl: 'support.html',
})
export class SupportPage {
  MESSAGEURL = 'http://localhost:3000/api/message';
  OKMSG = 'Mensagem Enviada';
  ERRORMSG = 'Não foi possível enviar a Mensagem, tente novamente';
  loading: Loading;
  message = '';
  opts: any;
  constructor(public navCtrl: NavController, public http: Http, public navParams: NavParams, private auth: LoginService, public alertCtrl: AlertController, public loadingCtrl: LoadingController) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad SupportPage');
  }

  showLoading() {
    this.loading = this.loadingCtrl.create({
      content: 'Please wait...',
      dismissOnPageChange: true
    });
    this.loading.present();
  }

  showMsg(text) {
    this.loading.dismiss();
 
    let alert = this.alertCtrl.create({
      title: '',
      subTitle: text,
      buttons: ['OK']
    });
    alert.present(prompt);
  }

  goHome(){
    this.navCtrl.setRoot(HomePage)
  }

  sendMessage(message){
    this.showLoading();
    let opts = this.auth.getOpts();
    let postParams = {value:message}

    //POST to send message for support
    return this.http.post(this.MESSAGEURL, postParams, opts)
      .map(res => res.json())
      .subscribe(data => {

        if (data) {
            this.message = data;
            this.showMsg(this.OKMSG);
            this.goHome();
        } else {
            this.message = null;
            this.showMsg(this.ERRORMSG);
            this.goHome();
        }
                
      }, (err) => {
        this.message = null;
        this.showMsg('Não foi possível enviar a Mensagem, tente novamente');
        console.log(err);
        this.goHome();
     });
        
  }

  cancel(){
    this.navCtrl.setRoot(HomePage)
  }

}
