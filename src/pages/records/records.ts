import { Http } from '@angular/http';
import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';

import { HomePage } from '../home/home';
import { LoginService } from '../../services/login-service';

@IonicPage()
@Component({
  selector: 'page-records',
  templateUrl: 'records.html',
})
export class RecordsPage {
  RECORDSURL = 'http://localhost:3000/api/history';
  records: any;
  username: any
  constructor(public navCtrl: NavController, public http: Http, public navParams: NavParams, private auth: LoginService) {

    let info = auth.getUserInfo();
    this.username = info['name'];
    this.getRecords();
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad RecordsPage');
  }

  backHome(course){
    this.navCtrl.setRoot(HomePage);
  }   

   public getRecords(){
    let options = this.auth.getOpts();

     return this.http.get(this.RECORDSURL, options)
      .map(res => res.json())
      .subscribe(data => {

        if (data) {
            this.records = data;
        } else {
            this.records = [];
        }
                
      }, (err) => {
        this.records = [];
        console.log(err)
     });

  }

}
